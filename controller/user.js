const { successResponse } = require('../helper/response')
const fs = require('fs')
const { json } = require('body-parser')


class User {
    constructor() {
       this.user = []
       const readUser = fs.readFileSync('./data/data.json', 'utf-8')
       if (readUser == ""){
           this.parsedUser = this.user
       }else{
           this.parsedUser = JSON.parse(readUser)
       }
    }

    getUser = (req, res) => {
        successResponse(res, 200, this.parsedUser, {total: this.parsedUser.length})
    }

    getDetailedUser = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.parsedUser[index])
    }
    
    insertUser = (req, res) => {
        const body = req.body
        const param = {
            "username": body.username,
            "password": body.password
        }
        this.parsedUser.push(param)
        fs.writeFileSync('./data/data.json', JSON.stringify(this.parsedUser))
        successResponse(res, 201, param)
            
    }
        
    updateUser = (req, res) => {
        const index = req.params.index
        const body = req.body
        
        if (this.parsedUser[index]) {
                this.parsedUser[index].username = body.username
                this.parsedUser[index].password = body.password

        fs.writeFileSync('./data/data.json', JSON.stringify(this.parsedUser))
        successResponse(res, 200, this.parsedUser[index])
        }else {
            successResponse(res, 422, null)
        }
    }

    deleteUser = (req, res) => {

        const index = req.params.index
    
        if (this.parsedUser.splice(index, 1)) {
            
        }

        successResponse(res, 200, null)
    }
}

module.exports = User