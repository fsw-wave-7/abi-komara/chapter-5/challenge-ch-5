const messages = {
    "200": "SUCCESS",
    "201": "USER DATA SAVED",
    "422": "FAILED TO PROCEED"
}

function successResponse( res,code,data,meta = {}) {
    res.status(code).json({
        data: data,
        meta: {
            code: code,
            messages: messages[code.toString()],
            ...meta
        }
    })
}

module.exports = { successResponse}