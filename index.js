const express = require ('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const api = require("./routes/api")
const path = require('path')

const app = express()
const port = 3000
const jsonParser = bodyParser.json()

app.use('/api', api)
app.use(jsonParser)
app.use(morgan('dev'))
app.use(express.static(__dirname + '/public'))

app.set('view engine', 'ejs')

app.get('/rps', function(req, res) {
    res.render(path.join(__dirname, './views/RPS'))
})

app.get('/', function(req, res) {
    res.render(path.join(__dirname, '/views/index'))
})

app.get('/check', (req, res) => {
    res.json({
        "Server": "Running"
    })
})

app.use((err, req, res, next) => {
    res.status(500).json ({
        status: "Failed to Process",
        errors: err.message
    })
})

app.use((req, res, next) => { /* kalo 404 gak usah pake parameter err*/
    res.status(404).json ({
        status: "Failed to Process",
        errors: "Something Wrong"
    })
})

app.listen(port, () => { console.log('Server Running') })