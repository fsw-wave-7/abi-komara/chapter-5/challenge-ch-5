const resultDesc = document.getElementById("resultDesc")
const resultID = document.getElementById("result")
const rockImg = document.getElementById("r")
const paperImg = document.getElementById("p")
const scissorImg = document.getElementById("s")
const refresh = document.getElementById("refresh")

// Generate Computer Choice

function getComputerChoice() {
    const choices = ["r", "p", "s"]
    const randomNumber = Math.floor(Math.random() * 3)
    return choices[randomNumber]
}

// Convert choice word

function desc(letter) {
    if (letter === "r")
        return "Rock"
    if (letter === "p")
        return "Paper"
    else
        return "Scissor"
}

//  Descripting win-lose-draw condition

function win(userChoice, getComputerChoice) {
    resultID.innerHTML = ` PLAYER 1 WIN`
    resultDesc.innerHTML = `You : ${desc(userChoice)} ||  Com : ${desc(getComputerChoice)}`
}

function lose(userChoice, getComputerChoice) {
    resultID.innerHTML = `COM WIN`
    resultDesc.innerHTML = `You : ${desc(userChoice)} || Com : ${desc(getComputerChoice)}`
}
function draw(userChoice, getComputerChoice) {
    resultID.innerHTML = `DRAW`
    resultDesc.innerHTML = `You : ${desc(userChoice)} || Com : ${desc(getComputerChoice)}`
}

// Comparing choices

function game(userChoice){
    const computerChoice = getComputerChoice()
    // console.log(" user choice >" + userChoice)
    // console.log("computer choice >" + computerChoice)
    switch (userChoice + computerChoice) {
        case "rs" :
        case "pr" :
        case "sp" :
            win(userChoice, computerChoice)
            console.log("user win")
            break
        case "rp" :
        case "ps" :
        case "sr" :
            lose(userChoice, computerChoice)
            console.log("user lose")
            break
        case "rr" :
        case "pp" :
        case "ss" :
            draw(userChoice, computerChoice)
            console.log("draw")
            break
    }
}

function main() {
    rockImg.addEventListener("click", function() {
    game("r")
})
    paperImg.addEventListener("click", function() {
    game("p")
})
    scissorImg.addEventListener("click", function() {
    game("s")
})
}

main()

// Reload function

function reload() {
    refresh.addEventListener("click", location.reload())
}