const express = require ('express')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const morgan = require('morgan')

const User = require('../controller/user')
const user = new User

let now = new Date();

function logger(req, res, next) {
    console.log(` ${now.toString()} Method: ${req.method}; URL: ${req.url}`)
    next()
}

const api = express.Router()
api.use(logger)

api.get('/user', user.getUser)
api.get('/user/:index', user.getDetailedUser)
api.post('/user', user.insertUser)
api.put('/user/:index', user.updateUser)
api.delete('/user/:index', user.deleteUser)

module.exports = api